# Collaborateurs  
POUPIN Romain - MAGNIEZ Clément

# Description
Gestionnaire léger pour Git (ou "preuve de concept de") basé sur l'API GitLab - projet web de L3.

# Repo 
https://gitlab.com/Romreventon/gitlite

# API 
https://docs.gitlab.com/ee/api/

# Installation
Pas de backend, donc aucune installation nécessaire

## Note 
Une clé API est bien sûr nécessaire pour manipuler GitLite ; elle est à renseigner dans la variable globale token présente en ligne 1 de custom\_scripts/list\_repos.js. A titre de démonstration, la clé API d'un compte créé pour l'occasion est fournie par défaut.

# Usage 
	git clone git@gitlab.com/Romreventon/gitlite.git
	npm install
	npm start
